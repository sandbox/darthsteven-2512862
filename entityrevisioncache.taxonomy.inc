<?php

/**
 * @file
 */

/**
 * Taxonomy term entity controller with persistent cache.
 */
class EntityrevisioncacheTaxonomyTermController extends TaxonomyTermController {
  public function resetCache(array $ids = NULL) {
    EntityrevisioncacheControllerHelper::resetEntityrevisioncache($this, $ids);
    parent::resetCache($ids);
  }
  public function load($ids = array(), $conditions = array()) {
    return EntityrevisioncacheControllerHelper::entityrevisioncacheLoad($this, $ids, $conditions);
  }
}

/**
 * Taxonomy vocabulary entity controller with persistent cache.
 */
class EntityrevisioncacheTaxonomyVocabularyController extends TaxonomyVocabularyController {
  public function resetCache(array $ids = NULL) {
    EntityrevisioncacheControllerHelper::resetEntityrevisioncache($this, $ids);
    parent::resetCache($ids);
  }
  public function load($ids = array(), $conditions = array()) {
    return EntityrevisioncacheControllerHelper::entityrevisioncacheLoad($this, $ids, $conditions);
  }
}
