<?php

/**
 * @file
 */

/**
 * Comment entity controller with persistent cache.
 */
class EntityrevisioncacheCommentController extends CommentController {
  public function resetCache(array $ids = NULL) {
    EntityrevisioncacheControllerHelper::resetEntityrevisioncache($this, $ids);
    parent::resetCache($ids);
  }
  public function load($ids = array(), $conditions = array()) {
    return EntityrevisioncacheControllerHelper::entityrevisioncacheLoad($this, $ids, $conditions);
  }
}
